package auxillary.actions.files;

import auxillary.Action;
import fs.Entity;

public class GetName implements Action<String> {
    private final Entity entity;

    public GetName(Entity entity) {
        this.entity = entity;
    }

    @Override
    public String execute() {
        return entity.getName();
    }
}
