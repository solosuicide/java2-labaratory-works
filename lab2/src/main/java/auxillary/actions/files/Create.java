package auxillary.actions.files;

import auxillary.Action;
import fs.entityImpl.Directory;
import fs.Entity;

public class Create implements Action<Boolean> {
    private final Directory directory;
    private final Entity entity;

    public Create(Directory directory, Entity entity) {
        this.directory = directory;
        this.entity = entity;
    }

    @Override
    public Boolean execute() {
        try {
            directory.insert(entity);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
