package auxillary.actions.files;

import auxillary.Action;
import fs.Entity;

public class GetPath implements Action<String> {
    private final Entity entity;

    public GetPath(Entity entity) {
        this.entity = entity;
    }

    @Override
    public String execute() {
        return entity.getPath();
    }
}