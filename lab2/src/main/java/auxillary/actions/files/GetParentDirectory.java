package auxillary.actions.files;

import auxillary.Action;
import fs.Entity;
import fs.entityImpl.Directory;

public class GetParentDirectory implements Action<Directory> {
    private final Entity entity;
    private final Directory directory;

    public GetParentDirectory(Entity entity, Directory directory) {
        this.entity = entity;
        this.directory = directory;
    }

    @Override
    public Directory execute() {
        return entity.getParentDirectory();
    }
}
