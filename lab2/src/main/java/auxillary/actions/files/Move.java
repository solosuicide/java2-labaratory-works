package auxillary.actions.files;

import auxillary.Action;
import fs.Entity;
import fs.entityImpl.Directory;

public class Move implements Action<Boolean> {
    private final Directory directory;
    private final Entity entity;

    public Move(Entity entity, Directory directory) {
        this.directory = directory;
        this.entity = entity;
    }

    @Override
    public Boolean execute() {
        try {
            entity.move(directory);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
