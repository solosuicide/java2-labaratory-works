package auxillary.actions.files;

import auxillary.Action;
import fs.Entity;

public class Delete implements Action<Boolean> {
    private final Entity entity;

    public Delete(Entity entity) {
        this.entity = entity;
    }

    @Override
    public Boolean execute() {
        try {
            entity.delete();
            return true;
        }catch (Exception e){
            return false;
        }
    }
}