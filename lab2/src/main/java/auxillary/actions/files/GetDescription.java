package auxillary.actions.files;

import auxillary.Action;
import fs.Entity;

public class GetDescription implements Action<String> {
    private final Entity entity;

    public GetDescription(Entity entity) {
        this.entity = entity;
    }

    @Override
    public String execute() {
        return entity.getDescription();
    }
}
