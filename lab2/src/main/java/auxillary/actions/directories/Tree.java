package auxillary.actions.directories;

import auxillary.Action;
import fs.entityImpl.Directory;

import java.io.IOException;

public class Tree implements Action<String> {
    private final Directory directory;

    public Tree(Directory directory) {
        this.directory = directory;
    }

    @Override
    public String execute() {
        try{
            return directory.tree();
        }catch (IOException e){
            return "{\"Exception\" : \"" + e.getMessage() + "\"}";
        }
    }
}
