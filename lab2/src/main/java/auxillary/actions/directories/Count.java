package auxillary.actions.directories;

import auxillary.Action;
import fs.entityImpl.Directory;

public class Count implements Action<Long> {
    private final Directory directory;
    private final Boolean recursive;


    public Count(Directory directory, Boolean recursive) {
        this.directory = directory;
        this.recursive = recursive;
    }


    @Override
    public Long execute() {
        return directory.count(recursive);
    }
}
