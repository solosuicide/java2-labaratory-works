package auxillary.actions.directories;

import auxillary.Action;
import fs.Entity;
import fs.entityImpl.Directory;

public class DeleteFile implements Action<Boolean> {
    private final Entity entity;
    private final Directory directory;

    public DeleteFile(Entity entity, Directory directory) {
        this.entity = entity;
        this.directory = directory;
    }


    @Override
    public Boolean execute() {
        try {
            directory.deleteFile(entity);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}