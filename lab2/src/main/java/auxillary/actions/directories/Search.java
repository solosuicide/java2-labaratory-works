package auxillary.actions.directories;

import auxillary.Action;
import fs.entityImpl.Directory;

import java.util.List;

public class Search implements Action<List<String>> {
    private final Directory directory;
    private final String pattern;

    public Search(Directory directory, String pattern) {
        this.directory = directory;
        this.pattern = pattern;
    }

    @Override
    public List<String> execute() {
        return directory.search(pattern);
    }
}
