package auxillary.actions.directories;

import auxillary.Action;
import fs.Entity;
import fs.entityImpl.Directory;

import java.util.List;

public class GetContent implements Action<List<Entity>> {
    private final Directory directory;

    public GetContent(Directory directory) {
        this.directory = directory;
    }

    @Override
    public List<Entity> execute() {
        return directory.getContent();
    }
}

