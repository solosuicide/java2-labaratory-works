package auxillary.actions.directories;

import auxillary.Action;
import fs.entityImpl.Directory;

public class CountByPattern implements Action<Long> {
    private final Directory directory;
    private final String pattern;
    private final Boolean recursive;

    public CountByPattern(Directory directory, String pattern, Boolean recursive) {
        this.directory = directory;
        this.pattern = pattern;
        this.recursive = recursive;
    }


    @Override
    public Long execute() {
        return directory.count(pattern, recursive);
    }
}
