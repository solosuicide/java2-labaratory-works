package auxillary;

import auxillary.actions.directories.DeleteFile;
import auxillary.actions.directories.GetContent;
import auxillary.actions.directories.Search;
import auxillary.actions.directories.Tree;
import auxillary.actions.files.*;
import fs.Entity;
import fs.entityImpl.BinaryEntity;
import fs.entityImpl.BufferEntity;
import fs.entityImpl.Directory;
import fs.entityImpl.LogEntity;

import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class Generator {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_PURPLE = "\u001B[35m";

    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {
        Directory root = new Directory("root");
        Controller controller = new Controller(10);

        for (int i = 0; i < 10; i++) {
            String dirName = "Dir" + i;
            Directory dir = new Directory(dirName);
            controller.submitFSAction(new Create(root, dir));
            if (i % 2 == 0) {
                submitBinaryFile(controller, dir, i);
            } else {
                submitSubDirectory(controller, dir, i);
            }
        }
        Future<String> future = controller.submitFSAction(new Tree(root));
        System.out.println(ANSI_RED + "JSON VIEW" + ANSI_RESET);
        System.out.println(ANSI_GREEN + "--------------------------------------------------------------------------------" + ANSI_RESET);
        System.out.println(ANSI_PURPLE + future.get() + ANSI_RESET);
        System.out.println(ANSI_GREEN + "--------------------------------------------------------------------------------" + ANSI_RESET);
        System.out.println(ANSI_RED + "FIND ALL BINARY FILES" + ANSI_RED);
        Future<List<String>> searchResult = controller.submitFSAction(new Search(root, "Binary"));
        System.out.println(ANSI_PURPLE + searchResult.get() + ANSI_RESET);
        System.out.println(ANSI_GREEN + "--------------------------------------------------------------------------------" + ANSI_RESET);
        controller.stop();
    }


    private static void submitBinaryFile(Controller controller, Directory parentDir, int num) {
        BinaryEntity binaryEntity = new BinaryEntity("Binary" + num);
        controller.submitFSAction(new Create(parentDir, binaryEntity));
    }

    private static void submitSubDirectory(Controller controller, Directory parentDir, int num) {
        Directory subDir = new Directory("SubDir" + num);
        controller.submitFSAction(new Create(parentDir, subDir));
        for (int j = 0; j < 3; j++) {
            controller.submitFSAction(new Create(subDir, new BufferEntity("Buffer" + num + ":" + j)));
            if (num % 6 == 0 && j % 2 == 0) {
                Directory subDir1 = new Directory("SubDir" + num + ":" + j);
                controller.submitFSAction(new Create(subDir, subDir1));
                for (int u = 0; u < 20; u++) {
                    controller.submitFSAction(new Create(subDir1,
                            new LogEntity("Log" + num + ":" + j + ":" + u)));
                }
            }
        }
    }
}
