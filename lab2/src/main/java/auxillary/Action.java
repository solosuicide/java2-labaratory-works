package auxillary;

public interface Action<T> {
     T execute();
}
