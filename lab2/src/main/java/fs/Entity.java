package fs;


import fs.entityImpl.Directory;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public abstract class Entity implements EntityOperations {
    private String name;
    private Directory parentDirectory;
    protected Lock mutex ;

    public Entity(String name) {
        this.name = name;
        mutex = new ReentrantLock();
    }

    @Override
    public String getName() {
        return name;
    }

    public Directory getParentDirectory() {
        return parentDirectory;
    }

    public void setParentDirectory(Directory newDirectory) {
        try {
            mutex.lock();
            this.parentDirectory = newDirectory;
        } finally {
            mutex.unlock();
        }
    }

    public void move(Directory newDirectory) {
        try {
            mutex.lock();
            if (parentDirectory != null)
                parentDirectory.deleteFile(this);
            newDirectory.insert(this);
        } finally {
            mutex.unlock();
        }
    }

    public void delete() {
        try {
            mutex.lock();
            if (parentDirectory != null) {
                parentDirectory.deleteFile(this);
            }
        } finally {
            mutex.unlock();
        }
    }

    public String getDescription() {
        return getName() + " (" + getClass().getSimpleName() + ")";
    }

    public String getPath() {
        if (parentDirectory == null) return "/" + getName();
        return ((Entity)parentDirectory).getPath() + "/" + getName();
    }
}

