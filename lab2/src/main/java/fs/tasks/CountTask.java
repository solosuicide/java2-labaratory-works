package fs.tasks;

import fs.entityImpl.Directory;
import fs.EntityOperations;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

public class CountTask extends RecursiveTask<Long> {
    private final Directory node;

    public CountTask(Directory node) {
        this.node = node;
    }

    @Override
    protected Long compute() {
        long sum = node.getContent().size();
        List<CountTask> subTasks = new LinkedList<>();

        for(EntityOperations child : node.getContent()) {
            if (child instanceof Directory) {
                CountTask task = new CountTask((Directory)child);
                task.fork();
                subTasks.add(task);
            }
        }

        for(CountTask task : subTasks) {
            sum += task.join();
        }

        return sum;
    }

}

