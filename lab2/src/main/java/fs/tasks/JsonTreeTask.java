package fs.tasks;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import fs.entityImpl.Directory;
import fs.EntityOperations;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

public class JsonTreeTask extends RecursiveTask<JsonNode> {
    private final Directory node;
    ObjectMapper mapper = new ObjectMapper();

    public JsonTreeTask(Directory node) {
        this.node = node;
    }

    @Override
    protected JsonNode compute() {
        ArrayNode root = mapper.createArrayNode();
        List<JsonTreeTask> subTasks = new LinkedList<>();

        for (EntityOperations child : node.getContent()) {
            JsonNode jNode = ToJson(child);
            if (child instanceof Directory) {
                JsonTreeTask task = new JsonTreeTask((Directory) child);
                task.fork();
                subTasks.add(task);
            }else {
                root.add(jNode);
            }
        }

        for (JsonTreeTask task : subTasks) {
            JsonNode jsonNode = task.join();
            root.add(jsonNode);
        }

        ObjectNode result = mapper.createObjectNode();
        result.put(node.getName(), root);

        return result;
    }

    private JsonNode ToJson(EntityOperations node){
        ObjectNode tmp = mapper.createObjectNode();
        tmp.put(node.getName(), node.getClass().getSimpleName());
        return tmp;
    }
}
