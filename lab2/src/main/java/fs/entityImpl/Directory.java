package fs.entityImpl;

import com.fasterxml.jackson.databind.ObjectMapper;
import fs.Entity;
import fs.tasks.CountTask;
import fs.tasks.JsonTreeTask;
import fs.tasks.SearchTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ForkJoinPool;

public class Directory extends Entity {
    private static final int DIR_MAX_ELEMENT = 100;

    private Set<Entity> children;

    public Directory(String name) {
        super(name);
        children = new LinkedHashSet<>();
    }

    public List<Entity> getContent() {
        try {
            mutex.lock();
            return new ArrayList<>(children);
        } finally {
            mutex.unlock();
        }
    }

    public void insert(Entity child) {
        try {
            mutex.lock();
            if (children.size() < DIR_MAX_ELEMENT) {
                children.add(child);
                child.setParentDirectory(this);
            }
        } finally {
            mutex.unlock();
        }
    }

    public void deleteFile(Entity child) {
        try {
            mutex.lock();
            children.remove(child);
        } finally {
            mutex.unlock();
        }
    }

    public List<String> search(String pattern){
        return  new ForkJoinPool().invoke(new SearchTask(this, pattern));
    }

    public Long count(boolean recursive){
        if(recursive){
            return new ForkJoinPool().invoke(new CountTask(this));
        }else{
            return (long) getContent().size();
        }
    }

    public String tree() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(new ForkJoinPool().invoke(new JsonTreeTask(this)));
    }

    public long count(String pattern, boolean recursive){
        if(recursive){
            return (long) search(pattern).size();
        }else{
            return getContent().stream().filter(x -> x.getName().contains(pattern)).count();
        }
    }
}
