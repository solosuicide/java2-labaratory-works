package fs.entityImpl;


import fs.Entity;

public class BinaryEntity extends Entity {
    private String content;

    public BinaryEntity(String name) {
        super(name);
        this.content = "";
    }

    public BinaryEntity(String name, String content) {
        super(name);
        this.content = content;
    }

    public String read() {
        return content;
    }
}
