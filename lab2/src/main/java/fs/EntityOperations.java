package fs;

import fs.entityImpl.Directory;

public interface EntityOperations {

    String getName();

    Directory getParentDirectory();

    void move(Directory newDirectory);

    void delete();

    String getDescription();

    String getPath();
}
