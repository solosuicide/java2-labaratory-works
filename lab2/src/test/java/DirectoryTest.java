import fs.*;
import fs.entityImpl.*;
import fs.entityImpl.BinaryEntity;
import fs.entityImpl.LogEntity;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DirectoryTest {
    @Test
    public void getContent_getContentEmpty_contentIsEmpty() {
        Directory root = new Directory("directory");
        assertEquals(Collections.emptyList(), root.getContent());
    }

    @Test
    public void insertAndGetContent_insertRightContent_contentExists() {
        Directory root = new Directory("root");
        Entity binEntity = new BinaryEntity("file.bin");
        Entity bufEntity = new BufferEntity("file.bf");
        Directory dataDir = new Directory("data");
        Directory logsDir = new Directory("logs");
        Entity logEntity = new LogEntity("file.log");

        root.insert(binEntity);
        root.insert(bufEntity);
        root.insert(dataDir);
        dataDir.insert(logsDir);
        logsDir.insert(logEntity);

        assertEquals(Arrays.asList(binEntity, bufEntity, dataDir), root.getContent());
        assertEquals(Collections.singletonList(logsDir), dataDir.getContent());
        assertEquals(Collections.singletonList(logEntity), logsDir.getContent());
    }

    @Test
    public void insertAndDelete_insertRightContent_contentIsDeleted() {
        Directory root = new Directory("root");
        Directory dataDir = new Directory("data");
        Entity bufEntity = new BufferEntity("file.bf");
        Entity binEntity = new BinaryEntity("file.bin");

        root.insert(dataDir);
        dataDir.insert(bufEntity);
        dataDir.insert(binEntity);

        bufEntity.delete();
        assertEquals(Collections.singletonList(binEntity), dataDir.getContent());

        dataDir.delete();
        assertEquals(Collections.emptyList(), root.getContent());
    }

    @Test
    public void search_ByPattern_contentExists() {
        Directory root = new Directory("root");
        Directory dataDir = new Directory("data");
        Entity bufEntity = new BufferEntity("file.bf");
        Entity binEntity = new BinaryEntity("file.bin");

        root.insert(dataDir);
        dataDir.insert(bufEntity);
        dataDir.insert(binEntity);

        List<String> actualPath = root.search("file");

        List<String> expectedPath = new ArrayList<>();
        expectedPath.add("/root/data/file.bf");
        expectedPath.add("/root/data/file.bin");

        assertTrue(expectedPath.equals(actualPath));

    }

    @Test
    public void count_ByPattern_contentExists() {
        Directory root = new Directory("root");
        Directory dataDir = new Directory("data");
        Entity bufEntity = new BufferEntity("file.bf");
        Entity binEntity = new BinaryEntity("file.bin");

        root.insert(dataDir);
        dataDir.insert(bufEntity);
        dataDir.insert(binEntity);

        Long count = dataDir.count("file", true);

        assertEquals(2, (long)count);
    }

    @Test
    public void count_ByRecursive_contentExists() {
        Directory root = new Directory("root");
        Directory dataDir = new Directory("data");
        Entity bufEntity = new BufferEntity("file.bf");
        Entity binEntity = new BinaryEntity("file.bin");

        root.insert(dataDir);
        dataDir.insert(bufEntity);
        dataDir.insert(binEntity);

        Long count = dataDir.count(true);

        assertEquals(2l, (long) count);
    }
}
