import fs.entityImpl.BinaryEntity;
import fs.entityImpl.Directory;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class BinaryEntityTest {

    @Test
    public void read_readEmptyContent_contentIsEmpty() {
        BinaryEntity file = new BinaryEntity("file.bin");
        assertEquals("", file.read());

        file = new BinaryEntity("file.bin", "");
        assertEquals("", file.read());
    }

    @Test
    public void read_readContent_rightContentExists() {
        BinaryEntity file = new BinaryEntity("file.bin", "content exists");
        assertEquals("content exists", file.read());
    }

    @Test
    public void getParentDirectory_fileWithoutParentDirectory_parentDirectoryIsNull() {
        BinaryEntity file = new BinaryEntity("file.bin", "content exists");
        Directory parent = file.getParentDirectory();
        assertNull(parent);
    }

    @Test
    public void getParentDirectory_fileWithParentDirectory_parentDirectoryIsNotNull() {
        Directory root = new Directory("directory");
        BinaryEntity file = new BinaryEntity("file.bin");
        file.move(root);
        assertEquals(root, file.getParentDirectory());
    }
}
