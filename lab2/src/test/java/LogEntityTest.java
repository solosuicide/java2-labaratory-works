import fs.entityImpl.Directory;
import fs.entityImpl.LogEntity;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class LogEntityTest {

    @Test
    public void read_emptyFile_contentIsNull(){
        LogEntity file = new LogEntity("file.log");
        assertEquals("", file.read());
    }

    @Test
    public void read_containsOneElement_contentNotEmpty(){
        LogEntity file = new LogEntity("file.log");

        file.write("first");

        assertEquals("first", file.read());

        // ensure that file has content after previous read
        assertEquals("first", file.read());
    }

    @Test
    public void read_containsSomeElements_contentNotEmpty(){
        LogEntity file = new LogEntity("file.log");

        String[] strings = { "one", "two", "three" };

        for (String item: strings) {
            file.write(item);
        }

        String expectedContent = String.join("\n", strings);
        assertEquals(expectedContent, file.read());
        assertEquals(expectedContent, file.read());
    }

    @Test
    public void getParent_fileWithoutParent_parentIsNull() {
        LogEntity file = new LogEntity("file.log");
        assertNull(file.getParentDirectory());
    }

    @Test
    public void getParent_fileWithParent_parentIsNotNull() {
        Directory root = new Directory("directory");
        LogEntity file = new LogEntity("file.bf");
        file.move(root);
        assertEquals(root, file.getParentDirectory());
    }
}
