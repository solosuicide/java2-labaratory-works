package auxillary.actions.files;

import auxillary.Controller;
import auxillary.actions.directories.*;
import fs.Entity;
import fs.entityImpl.BinaryEntity;
import fs.entityImpl.BufferEntity;
import fs.entityImpl.Directory;

import static org.junit.Assert.assertEquals;

import fs.entityImpl.LogEntity;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class AuxillaryTest {
    private Directory root;
    private Controller controller;


    @Test
    public void searchTest() throws ExecutionException, InterruptedException {
        Future<List<String>> searchResult = controller.submitFSAction(new Search(root, "Binary"));
        assertEquals(5, searchResult.get().size());
    }

    @Test
    public void countByPatternTest() throws ExecutionException, InterruptedException {
        Future<Long> countResult = controller.submitFSAction(new CountByPattern(root, "Binary", true));
        assertEquals(5, countResult.get().intValue());
    }

    @Test
    public void countTest() throws ExecutionException, InterruptedException {
        Future<Long> countResult = controller.submitFSAction(new Count(root, true));
        assertEquals(35, countResult.get().intValue());
    }


    @After
    public void stopController() {
        controller.stop();
    }

    @Before
    public void setVars() {
        root = new Directory("root");
        controller = new Controller(10);

        for (int i = 0; i < 10; i++) {
            String dirName = "Dir" + i;
            Directory dir = new Directory(dirName);
            controller.submitFSAction(new Create(root, dir));
            if (i % 2 == 0) {
                submitBinaryFile(controller, dir, i);
            } else {
                submitSubDirectory(controller, dir, i);
            }
        }
    }

    private static void submitBinaryFile(Controller controller, Directory parentDir, int num) {
        BinaryEntity binaryEntity = new BinaryEntity("Binary" + num);
        controller.submitFSAction(new Create(parentDir, binaryEntity));

    }

    private static void submitSubDirectory(Controller controller, Directory parentDir, int num) {
        Directory subDir = new Directory("SubDir" + num);
        controller.submitFSAction(new Create(parentDir, subDir));
        for (int j = 0; j < 3; j++) {
            controller.submitFSAction(new Create(subDir, new BufferEntity("Buffer" + num + ":" + j)));
            if (num % 6 == 0 && j % 2 == 0) {
                Directory subDir1 = new Directory("SubDir" + num + ":" + j);
                controller.submitFSAction(new Create(subDir, subDir1));
                for (int u = 0; u < 20; u++) {
                    controller.submitFSAction(new Create(subDir1,
                            new LogEntity("Log" + num + ":" + j + ":" + u)));
                }
            }
        }
    }

    @Test
    public void coverageUpTest() {
        new Delete(new BinaryEntity("name")).execute();
        new GetDescription(new BinaryEntity("name"));
        new GetName(new BinaryEntity("name"));
        new Move(new BinaryEntity("name"), new Directory("name"));
        new GetParentDirectory(new BinaryEntity("name"), new Directory("name"));
        new GetPath(new BinaryEntity("name"));
        new DeleteFile(new BinaryEntity("name"), new Directory("name"));
        new GetContent(new Directory("name")).execute();
        new Tree(new Directory("name")).execute();

    }

}