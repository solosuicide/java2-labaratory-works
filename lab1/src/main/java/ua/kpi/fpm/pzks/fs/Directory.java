package ua.kpi.fpm.pzks.fs;

import java.util.*;

public class Directory extends Entity {
    private static final int MAX_ELEMS = 10;
    private Map<String, Entity> children = new HashMap<>(MAX_ELEMS);
    private final Object monitor = new Object();

    protected Directory(String name, Directory parent) {
        super(name, parent);
    }

    private Directory() {
        super("root");
    }

    public static Directory create(String name, Directory parent) {
        if (parent == null) {
            return new Directory();
        }
        return new Directory(name, parent);
    }

    public boolean containsChild(String name) {
        return children.containsKey(name);

    }

    public void addChild(Entity child) {
        synchronized (monitor) {
            if (this.children.size() < MAX_ELEMS)
                this.children.put(child.getName(), child);
        }

    }

    @Override
    protected void move(Directory destination) {
        super.move(destination);
    }

    @Override
    protected void delete(String name) {
        synchronized (monitor) {
            children.remove(name);
        }
    }

    public List<Entity> list() {
        synchronized (monitor) {
            return new ArrayList<>(children.values());
        }
    }
}
