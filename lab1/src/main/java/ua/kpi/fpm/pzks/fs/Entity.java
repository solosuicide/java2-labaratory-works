package ua.kpi.fpm.pzks.fs;

public abstract class Entity {
    protected String name;
    protected Directory parent;
    protected String path;
    private final Object monitor = new Object();

    protected Entity(String name, Directory parent) {
        this(name);
        if (name.isEmpty()) {
            throw new IllegalArgumentException("Name could not be null or zero length");
        }
        if (parent == null) {
            throw new IllegalArgumentException("Parent directory could not be null");
        }

        if (parent.containsChild(name)) {
            throw new IllegalArgumentException("Element with same name already exists");
        }
        this.parent = parent;
        parent.addChild(this);

    }

    protected Entity(String name) {
        if (name.isEmpty()) {
            throw new IllegalArgumentException("Name could not be null or zero-length and contain '/'");
        }
        this.name = name;
    }

    protected void delete(String name) {
        parent.delete(name);
    }

    protected void move(Directory destination) {
        if (destination == null) {
            throw new IllegalArgumentException("Destination could not be null");
        }
        synchronized (monitor) {
            parent.delete(this.name);
            destination.addChild(this);
        }
    }


    public String getName() {
        return name;
    }

    public Directory getParent() {
        return parent;
    }

    public String getPath() {
        return path;
    }
}
