package ua.kpi.fpm.pzks.fs;

import ua.kpi.fpm.pzks.fs.*;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class Task2 {
    public static void main(String[] args) throws URISyntaxException, IOException {
        Directory root = Directory.create("root", null);
        Directory userDir = Directory.create("user", Directory.create("home", root));
        Directory sysDir = Directory.create("sys", root);
        BinaryFile binaryFile = BinaryFile.create("data.bin", sysDir, "dat".getBytes());
        BufferFile bufferFile = BufferFile.<Character>create("buff.sys", sysDir);
        LogFile logFile = LogFile.create("sample.log", root, "some log\n");
        UrlBinaryFile urlBinaryFile = UrlBinaryFile.create("google", userDir, new URI("https://www.codota.com/code/java/methods/org.apache.commons.io.IOUtils/toByteArray"));

        showDirectoriesEntries(root, userDir, sysDir);
        logFile.move(userDir);
        urlBinaryFile.move(root);
        bufferFile.move(userDir);
        sysDir.delete("data.bin");
        userDir.move(sysDir);
        showDirectoriesEntries(root, userDir, sysDir);

    }

    private static void showDirectoriesEntries(Directory root, Directory userDir, Directory sysDir) {
        System.out.println("ROOT");
        root.list().forEach(entity -> System.out.println(entity.name));
        System.out.println("********************************");
        System.out.println("SYSTEM");
        sysDir.list().forEach(entity -> System.out.println(entity.name));
        System.out.println("********************************");
        System.out.println("USER");
        userDir.list().forEach(entity -> System.out.println(entity.name));
        System.out.println("********************************");
    }
}
